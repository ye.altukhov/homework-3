import { Controller, Get } from "@nestjs/common";
import { HttpService } from "@nestjs/axios";

import { AppService } from "./app.service";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService,
              private readonly httpService: HttpService
  ) {
  }

  async trackEvent() {
    const nbu = await this.httpService.axiosRef.get(`https://bank.gov.ua/NBU_Exchange/exchange?json`);
    const grn = nbu.data.find(el => el.CurrencyCodeL === "USD" && el).Amount.toString();
    const random = (min, max) => Math.floor(Math.random() * (max - min)) + min;
    const measurement_id = "G-VF3H5VCQ7H";
    const api_secret = "NBJRUb6vQGKnWLvonsaUew";
    const response = await this.httpService.axiosRef.post(`https://www.google-analytics.com/mp/collect?measurement_id=${measurement_id}&api_secret=${api_secret}`,
      JSON.stringify({
        client_id: random(1000000000, 2147483647) + "." + new Date().getTime(),
        events: [{
          name: "get_data",
          params: {
            grn
          }
        }]
      })
    );
    return response;
  };

  @Get()
  async getHello() {
    try {
      const response = await this.trackEvent();
      return response.data;
    } catch (e) {
      return e;
    }
  }
}
